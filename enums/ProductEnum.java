package enums;

/**
 * Created by Mc on 4/28/14.
 */
public enum ProductEnum {
    TABLE, LAMP, CHAIR;

    public String toString() {
        String s = super.toString();
        return (s.substring(0, 1) + s.substring(1).toLowerCase());
    }
}
