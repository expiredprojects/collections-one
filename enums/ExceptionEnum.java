package enums;

/**
 * Created by Mc on 5/3/14.
 */
public enum ExceptionEnum {
    NOT_VALID_PRODUCT_NAME, NOT_VALID_QUANTITY, NOT_VALID_PRICE, FILE_NOT_FOUND, NOT_VALID_NUMBER_FORMAT;

    public Exception toExcept(String error) {
        String s = super.toString();
        return new Exception(s.substring(0, 1) + s.substring(1).toLowerCase() + ":" + error);
    }
}
