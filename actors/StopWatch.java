package actors;

import java.io.Serializable;

/**
 * Created by Mc on 4/28/14.
 */
public class StopWatch {
    private String message;
    private long startTime = 0;
    private long endTime = 0;
    private long totalTime = 0;

    public void go(String message) {
        this.message = message;
        startTime = System.nanoTime();
    }

    public void stop() {
        endTime = System.nanoTime();
        totalTime = endTime - startTime;
    }

    //Number of seconds
    public int getResult() {
        return (int) (totalTime / Math.pow(10, 9));
    }

    public String getResult(boolean full) {

        return String.valueOf(full ? (message + " " + getResult()) : getResult());
    }

    public String getResult(boolean full, int precision) {
        double fullTime = totalTime / Math.pow(10, 9);
        if (precision > -1 && precision < 10) {
            double power = Math.pow(10, precision);
            fullTime = Math.round(fullTime * power) / power;
        }
        return String.valueOf(full ? (message + " " + fullTime) : fullTime);
    }
}
