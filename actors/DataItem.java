package actors;

import enums.ExceptionEnum;
import enums.ProductEnum;

import java.io.Serializable;

/**
 * Created by Mc on 4/28/14.
 */
public class DataItem implements Serializable {
    private ProductEnum productName;
    private int quantity = 1;
    private float price = 10;

    public DataItem(String source) throws Exception {
        String[] parts = source.split(";");
        setProductName(parts[0]);
        setQuantity(parts[1]);
        setPrice(parts[2]);
    }

    public void setProductName(String productName) throws Exception {
        if (productName.toLowerCase().equals(ProductEnum.CHAIR.toString().toLowerCase())) {
            this.productName = ProductEnum.CHAIR;
        }
        else if (productName.toLowerCase().equals(ProductEnum.LAMP.toString().toLowerCase())) {
            this.productName = ProductEnum.LAMP;
        }
        else if (productName.toLowerCase().equals(ProductEnum.TABLE.toString().toLowerCase())) {
            this.productName = ProductEnum.TABLE;
        }
        else throw ExceptionEnum.NOT_VALID_PRODUCT_NAME.toExcept(productName);
    }

    public ProductEnum getProductName() {
        return productName;
    }

    private void setQuantity(String quantity) throws Exception {
        int q;
        try {
            q = Integer.parseInt(quantity);
        }
        catch (NumberFormatException e) {
            throw ExceptionEnum.NOT_VALID_NUMBER_FORMAT.toExcept(quantity);
        }
        if (q >= 1 && q <= 20) {
            this.quantity = q;
        }
        else throw ExceptionEnum.NOT_VALID_QUANTITY.toExcept(quantity);
    }

    public int getQuantity() {
        return quantity;
    }

    public void setPrice(String price) throws Exception {
        float p;
        try {
            p = Float.parseFloat(price.replace(',', '.'));
        }
        catch (NumberFormatException e) {
            throw ExceptionEnum.NOT_VALID_NUMBER_FORMAT.toExcept(price);
        }
        if (p >= 10 && p <= 110) {
            this.price = p;
        }
        else throw ExceptionEnum.NOT_VALID_PRICE.toExcept(price);
    }

    public float getPrice() {
        return price;
    }
}
