package comps;

import actors.DataItem;

import java.util.Comparator;

/**
 * Created by Mc on 5/4/14.
 */
public class QuantityDescComp implements Comparator<DataItem> {
    @Override
    public int compare(DataItem a, DataItem b) {
        return a.getQuantity() < b.getQuantity() ? 1 : a.getQuantity() == b.getQuantity() ? 0 : -1;
    }
}
