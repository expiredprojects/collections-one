package comps;

import actors.DataItem;

import java.util.Comparator;

/**
 * Created by Mc on 5/4/14.
 */
public class PriceAscComp implements Comparator<DataItem> {
    @Override
    public int compare(DataItem a, DataItem b) {
        return a.getPrice() < b.getPrice() ? -1 : a.getPrice() == b.getPrice() ? 0 : 1;
    }
}
