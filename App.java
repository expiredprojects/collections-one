import actors.DataItem;
import comps.PriceAscComp;
import comps.QuantityDescComp;
import enums.ExceptionEnum;
import actors.StopWatch;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by Mc on 4/7/14.
 */
public class App {
    private static final String sourcePath = "Items.txt";
    private static final String serialPath = "Data.s";
    private static final float cost = (float) 47.18;

    public static void main(String[] args) {
        try {
            ArrayList<DataItem> arrayList = new ArrayList<DataItem>();
            DataItem[] array = new DataItem[0];
            StopWatch stopWatch = new StopWatch();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String command = "";
            while (command != "exit") {
                System.out.println("Available commands:");
                System.out.println("- import (import ArrayList from file)");
                System.out.println("- sortAsc (sort ascending of prices)");
                System.out.println("- sortDesc (sort desc of quantity)");
                System.out.println("- displayCheap (display 10 cheapest items)");
                System.out.println("- displayMost (display 10 most numerous items)");
                System.out.println("- displayCount (display count of items with cost " + cost + ")");
                System.out.println("- serialize");
                System.out.println("- deserialize");
                System.out.println("- convert - convert ArrayList to Array");
                System.out.println("- aSortAsc (sort array ascending of price)");
                System.out.println("- aSortDesc (sort array desc of quantity)");
                System.out.println("- aDisplayCheap (display 10 cheapest items from array)");
                System.out.println("- aDisplayMost (display 10 most numerous items from array)");
                System.out.println("- aDisplayCount (display count of items with cost " + cost + " from array)");
                System.out.println("- exit (quits program loop)");
                try {
                    command = bufferedReader.readLine();
                    String[] commandParts = command.split(" ");
                    stopWatch.go(commandParts[0]);
                    if (commandParts[0].equals("import")) {
                        arrayList = importList(sourcePath);
                    }
                    else if (commandParts[0].equals("sortAsc")) {
                        Collections.sort(arrayList, new PriceAscComp());
                        System.out.println("Sorted ascending by price.");
                    }
                    else if (commandParts[0].equals("sortDesc")) {
                        Collections.sort(arrayList, new QuantityDescComp());
                        System.out.println("Sorted descending by quantity.");
                    }
                    else if (commandParts[0].equals("displayCheap")) {
                        Collections.sort(arrayList, new PriceAscComp());
                        for (int i = 0, size = arrayList.size(); i < 10 && i < size; i++) {
                            System.out.println(i + " " + arrayList.get(i).getProductName() + " " + arrayList.get(i).getPrice());
                        }
                    }
                    else if (commandParts[0].equals("displayMost")) {
                        Collections.sort(arrayList, new QuantityDescComp());
                        for (int i = 0, size = arrayList.size(); i < 10 && i < size; i++) {
                            System.out.println(i + " " + arrayList.get(i).getProductName() + " " + arrayList.get(i).getQuantity());
                        }
                    }
                    else if (commandParts[0].equals("displayCount")) {
                        int i = 0;
                        for (DataItem dataItem : arrayList) {
                            if (dataItem.getPrice() == cost) {
                                i++;
                            }
                        }
                        System.out.println("Total count of items with " + cost + " price is " + i);
                    }
                    else if (commandParts[0].equals("serialize")) {
                        FileOutputStream fileOut = new FileOutputStream(serialPath);
                        ObjectOutputStream out = new ObjectOutputStream(fileOut);
                        out.writeObject(arrayList);
                        out.close();
                        fileOut.close();
                        System.out.println("Serialization finished.");
                    }
                    else if (commandParts[0].equals("deserialize")) {
                        FileInputStream fileIn = new FileInputStream(serialPath);
                        ObjectInputStream in = new ObjectInputStream(fileIn);
                        arrayList = (ArrayList<DataItem>) in.readObject();
                        in.close();
                        fileIn.close();
                        System.out.println("Deserialization finished.");
                    }
                    else if (commandParts[0].equals("convert")) {
                        array = arrayList.toArray(new DataItem[arrayList.size()]);
                        System.out.println("Conversion finished.");
                    }
                    else if (commandParts[0].equals("aSortAsc")) {
                        Arrays.sort(array, new PriceAscComp());
                        System.out.println("Array sorted ascending by price.");
                    }
                    else if (commandParts[0].equals("aSortDesc")) {
                        Arrays.sort(array, new QuantityDescComp());
                        System.out.println("Array sorted descending by quantity.");
                    }
                    else if (commandParts[0].equals("aDisplayCheap")) {
                        Arrays.sort(array, new PriceAscComp());
                        for (int i = 0, size = array.length; i < 10 && i < size; i++) {
                            System.out.println(i + " " + array[i].getProductName() + " " + array[i].getPrice());
                        }
                    }
                    else if (commandParts[0].equals("aDisplayMost")) {
                        Arrays.sort(array, new QuantityDescComp());
                        for (int i = 0, size = array.length; i < 10 && i < size; i++) {
                            System.out.println(i + " " + array[i].getProductName() + " " + array[i].getQuantity());
                        }
                    }
                    else if (commandParts[0].equals("aDisplayCount")) {
                        int i = 0;
                        for (DataItem dataItem : array) {
                            if (dataItem.getPrice() == cost) {
                                i++;
                            }
                        }
                        System.out.println("Total count of items with " + cost + " price is " + i);
                    }
                    else if (commandParts[0].equals("exit")) {
                        System.out.print("Bye.");
                        break;
                    }
                    else {
                        System.out.println("Invalid command.");
                        continue;
                    }
                    stopWatch.stop();
                    System.out.println("Time during " + stopWatch.getResult(true, 2));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static ArrayList importList(String fileName) throws Exception {
        ArrayList<DataItem> arrayList = new ArrayList<DataItem>();
        try {
            Scanner reader = new Scanner(new File(fileName));
            int i = 0;
            while(reader.hasNextLine()) {
                String line = reader.nextLine();
                try {
                    i++;
                    arrayList.add(new DataItem(line));
                }
                catch(Exception e) {
                    System.out.println("Line: " + i + " " + e.getMessage());
                }
            }
            reader.close();
        }
        catch (FileNotFoundException e) {
            throw ExceptionEnum.FILE_NOT_FOUND.toExcept(fileName);
        }
        return arrayList;
    }
}
